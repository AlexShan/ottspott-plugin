$(function () {

  $(document).ready(function(){

      $('[data-go-to-tour-step]').on('click', function(){
          var tour = $(this).data('go-to-tour-step');
          $('[data-tour-step]').removeClass('show');
          $('[data-tour-step="'+ tour +'"]').addClass('show');
      });

      $('[data-finish-tour]').on('click', function(){
          $('[data-tour-step]').removeClass('show');
      });
  });

});
