var gulp        = require('gulp'),
    browserSync = require('browser-sync').create(),
    fileinclude = require('gulp-file-include'),
    /* sass ot css stuff */
    sass        = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    /* js stuff */
    concat      = require('gulp-concat'),
    rename      = require('gulp-rename'),
    uglify      = require('gulp-uglify');


/*

  Iconfont Stuff.
  NOTE: Use glyphter.com to generate custom icon font

*/

/* javascripts tasks */
var jsFiles = [
      './app/js/vendor/jquery.min.js', /* this is jquery v3.1.1 */
      './app/js/vendor/tether.min.js',
      './app/js/vendor/poper.js', /* this is jquery v3.1.1 */
      './app/js/vendor/bootstrap.min.js',
      './app/js/**/*.js'
    ],
    jsDest = './dist/js';

gulp.task('scripts', function() {
    return gulp.src(jsFiles)
              .pipe(concat('app.js'))
              .pipe(gulp.dest(jsDest))
              .pipe(rename('app.min.js'))
              // .pipe(uglify())
              .pipe(gulp.dest(jsDest));
});


/* sass tasks */
gulp.task('sass', function() {
    return gulp.src("./app/scss/*.scss")
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest("./dist/css"))
        .pipe(browserSync.stream());
});

/* fileinclude tasks */
gulp.task('fileinclude', function() {
  gulp.src(['./app/templates/*.html'])
    .pipe(fileinclude())
    .pipe(gulp.dest('./dist/'));
});

/*

  One tasl to rule them all, one task to find them,
  One task to bring them all and in the darkness bind them :)

*/
gulp.task('serve', ['sass', 'fileinclude', 'scripts'], function() {

  browserSync.init({
      server: "./dist",
      port: 9000
  });

  gulp.watch("./app/js/**/*.js", ['scripts'])
  gulp.watch("./app/js/**/*.js").on('change', browserSync.reload);

  gulp.watch("./app/templates/*.html", ['fileinclude'])
  gulp.watch("./app/templates/**/*.html", ['fileinclude'])
  gulp.watch("./app/templates/*.html").on('change', browserSync.reload);

  gulp.watch("./app/scss/**/*.scss", ['sass']);
  gulp.watch("./app/scss/*.scss", ['sass']);
  // gulp.watch("./app/templates/**/*.html").on('change', browserSync.reload);
});
gulp.task('default', ['serve']);
